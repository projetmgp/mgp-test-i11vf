"""
  Copyright (c) 2016 Sokhna Kandji and Kévin Felix
  Released under the MIT License (see license.txt)
"""
import argparse
import sys


import diane

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('command', choices=['describe','add','update','del','list-full'],
                    help="Command to be executed on Diane")
parser.add_argument('targetId', type=int, nargs='?',
              help="Target ID for describe commands")
parser.add_argument("-p", "--position", type=float, nargs=3,
                    help="Geographic coordinates (lat./long./alt.) for update command")


def main(argv):

  # Parse the command-line flags.
  flags = parser.parse_args(argv[1:])

  # Check arguments consistency
  if flags.command in ['describe'] and flags.targetId is None:
    parser.print_help()
    print 'Please provide a target ID'
    return
  if flags.command in ['update'] and flags.position is None:
    parser.print_help()
    print 'Please provide a position for target ID'
    return
  if flags.command in ['del'] and flags.targetId is None:
    parser.print_help()
    print 'Please provide a target ID'
    return


  # Create a Selecter
  selecter = diane.SessionControl()
  # Target description
  if (flags.command == 'describe'):
    # Create request arguments dictionnary according to command line parameters
    create_args = {}
    if (flags.targetId is not None):
        create_args['target'] = flags.targetId
    print selecter.targetDescribe(create_args['target'])

  # Target creation
  elif (flags.command == 'add'):
    # Create request arguments dictionnary according to command line parameters
    create_args = {}
    if (flags.targetId is not None):
        create_args['target'] = flags.targetId

    if (flags.position is not None):
        create_args['position'] = flags.position
        #print create_args['position']
        selecter.addTarget(create_args['position'])
        print selecter.addTarget(create_args['position'])

  # Update position and/or orientation
  elif (flags.command == 'update'):
    update_args = {}
    create_args = {}
    if (flags.position is not None):
        update_args['position'] = flags.position
    if (flags.targetId is not None):
        create_args['target'] = flags.targetId
        #print create_args['target']
    print selecter.updateTarget(create_args['target'], update_args['position'])

  elif (flags.command == 'del'):
      create_args = {}
      if (flags.targetId is not None):
        create_args['target'] = flags.targetId
      print selecter.delTarget(create_args['target'])

  elif (flags.command == 'list-full'):

      print selecter.ListTargetIds(True)


if __name__ == '__main__':
  main(sys.argv)