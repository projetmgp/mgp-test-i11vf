import falcon
import ressource
"""Geoloc Service Application

  Copyright (c) 2016 Sokhna Kandji and Kévin Felix
  Released under the MIT License (see license.txt)
"""

# entry point for exec (main)
api = application = falcon.API()

# fixe globale system path for application
storage_path = 'storage'

# Link on Collection and Item representation
target_collection = ressource.Targets(storage_path)
target = ressource.Target(storage_path)

# add URI path access
api.add_route('/targets', target_collection)
api.add_route('/targets/{targetID}', target)
