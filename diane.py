#!/usr/bin/python
# -*- coding: latin-1 -*-
"""API for Geoloc service access

  Copyright (c) 2016 Sokhna Kandji and Kévin Felix
  Released under the MIT License (see license.txt)

@author: Kévin Félix, Sokhna Kandji
@status: Development
"""

import json


class SessionControl:
    """
    Main function for API Diane
    """
    def addTarget(self, args):
        """Create a new target.
        Synchronous.
        Args:
            args (Dictionnary): creation request attributes.
        Returns:
            int: newly created target ID.
        """

        return 'nous avons bien enregistré la cible avec les positions {0}'.format(args)

    def delTarget(self, targetId):
        """Destroy a given target.
        Asynchronous.
        Args:
            targetId (int): ID of the target to be deleted.
        Returns:
            str: status delete operation information
        """
        return 'nous avons supprimé la cible {0}'.format(targetId)

    def ListTargetIds(self,full=False):
        """Get target id list
        Synchronous.
        Args:
            full (bool): list all targets if True, only non-user targets otherwise.
        Returns:
            Array[int]: ids of existing targets.
        """
        response = [{"name": "targetTest", "altitude": 0.0, "longitude": -4.572, "latitude": 48.3574, "id": "123", "channel": 1234},{"name": "targetTest2", "altitude": 0.0, "longitude": -4.571883, "latitude": 48.3569, "id": "124", "channel": 1234}]

        targets = []
        for element in response:
            targets.append(element)
        return targets


    def updateTarget(self, targetId,args):
        """ update target parameters
        Asynchronous.
        Args:
            targetId (int): ID of the target to be deleted
            args {parameter: value}: jason object of parameters to update
        Returns:
            str: status update operation information
        """
        return 'nous avons modifié la target {1} avec les positions {0}'.format(args,targetId)

    def targetDescribe(self, targetId):
        """ Describe a target.
        Synchronous.
        Args:
            targetId (int): ID of the target to be described.
        Returns:
            targetdescription {parameter: value}: target informations associate to targetId
        """
        targetdescription = {"name": "targetTest", "altitude": 0.0, "longitude": -4.572067, "latitude": 48.3567, "id": "123", "channel": 1234}
        return targetdescription

class Error(Exception):
  """Base class for exceptions in this module."""
  pass

class UnavailablePlugin(Error):
    """Raised when Diane plugin is not available on Nayru server
    """

    def __init__(self):
        pass

    def __str__(self):
        return 'Nayrus plugin unavailable on server'

