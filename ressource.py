"""
  Copyright (c) 2016 Sokhna Kandji and Kévin Felix
  Released under the MIT License (see license.txt)
"""
import os
import time
import uuid
import decimal

import json
import falcon

def _generate_id():
    return str(uuid.uuid4())

class Targets(object):
    """Collection targets
    """
    def __init__(self, storage_path):
        """
        Args:
            storage_path(str): locale path where will be store the target collection
        """
        self.storage_path = storage_path

    def on_post(self, req, resp):
        """ POST http request
        To add a new target at the collection
        """
        target_id = _generate_id()
        target_path = os.path.join(self.storage_path, target_id)

        target = json.loads(req.stream.read())
        target["id"] = target_id
        
        with open(target_path, 'w') as target_file:
            target_file.write(json.dumps(target))

        resp.status = falcon.HTTP_201
        resp.location = '/targets' + target_id

    def on_get(self, req, resq):
        """ GET http request
        To add list of targets in the collection
        """
        return None

    def on_delete(self, req, resq):
        return None

class Target(object):
    
    def __init__(self, storage_path):
        self.storage_path = storage_path

    def on_get(self, req, resp, targetID):
        """ GET http request
        To get target description
        """
        target_path = os.path.join(self.storage_path, targetID)
        with open (target_path, 'r') as target_file:
            resp.body = target_file.read()