#!/usr/bin/python
# -*- coding: latin-1 -*-
"""
  Copyright (c) 2016 Sokhna Kandji and Kévin Felix
  Released under the MIT License (see license.txt)

@author: Kévin Félix, Sokhna Kandji
@status: Development
"""

#
#
#
from math import acos, sin, cos, radians, atan
import diane
import dazzl
import logging
from geopy.distance import vincenty


####
class SessionRegina(object):

    def channelVideocasterIds(self, channelId):
        """Describe a channel and gives the Ids of videocasters
        @param channel: channel ID
        @return ids: ids of all videocasters
        """
        # Try get channel description
        logger = logging.getLogger('test')
        ids = []
        with dazzl.Session('52.48.244.76',logger) as session:
            session.joinChannel(channelId,'director')
            videocasts = session.channelDescribe(channelId)["videocasts"]
            for videocast in videocasts:
                ids.append(videocast["id"])

            session.leaveChannel()
        return ids

    def channelvideocastsdescription(self, channel):
        """Videocast description for channel
        @param channel: channel ID
        @return
            videocastdescription: videocasts descriptions
        """
        logger = logging.getLogger('test')
        videocastsposition = []
        videocastspositionformat = {"id": 0, "latitude": 0, "longitude":0}
        with dazzl.Session('52.48.244.76',logger) as session:
            videocastsIds = (self.channelVideocasterIds(channel))
            for i in range(0,len(videocastsIds),1):
                videocastdesc = session.videocastDescribe(channel,videocastsIds[i])
                latitude = videocastdesc["latitude"]
                longitude = videocastdesc["longitude"]
                beta =videocastdesc["beta"]
                alpha = videocastdesc["alpha"]
                gamma = videocastdesc["gamma"]
                idv = videocastdesc["id"]
                videocastspositionformat = {"id":idv,"latitude":latitude,"longitude":longitude,"beta":beta,"alpha":alpha,"gamma":gamma}
                videocastsposition.append(videocastspositionformat)

        return videocastsposition


    def videocastSelected (self, target, channel):
        """

        Args:
            target: target ID
            channel: channel ID

        Returns:
            The videocast selected for the main view

        """
        #rayon de la terre en km
        rt = 6371
        distancemin =5100

        posTarget =self.targetdescription(target)
        lat_2 = posTarget["latitude"]
        lat2 = radians(lat_2)
        lon_2 = posTarget["longitude"]
        lon2 = radians(lon_2)

        addrdazzl = '52.48.244.76'
        logger = logging.getLogger('test')
        with dazzl.Session(addrdazzl,logger) as session:
            session.joinChannel(channel,'director')

            posVideocast = self.channelvideocastsdescription(channel)
            for i in range(0,len(posVideocast),1):
                lat_1 = posVideocast[i]["latitude"]
                lat1 = radians(lat_1)
                lon_1 = posVideocast[i]["longitude"]
                lon1 = radians(lon_1)
                beta = posVideocast[i]["gamma"]
                alpha = posVideocast[i]["alpha"]
                gamma = posVideocast[i]["gamma"]

                d = acos(sin(lat1)*sin(lat2)+cos(lat1)*cos(lat2)*cos(lon1-lon2))*rt

                print(d)
                if d < distancemin:

                    idvideocastselected = posVideocast[i]["id"]
                    print(d)
                    print'videocast [{0}] selected'.format(idvideocastselected)
                    break

            return idvideocastselected

    def videocastDirection(self,channel, target):
        """

        Args:
            channel: channel ID
            target: target ID

        Returns
            angleCV: The compass heading of videocast

        """
        theta = 40
        posTarget =self.targetdescription(target)
        lat_c = posTarget["latitude"]
        latc = radians(lat_c)
        lon_c = posTarget["longitude"]
        lonc = radians(lon_c)
        #cartesian coordinate point of target
        xc = cos(lat_c)*cos(lon_c)
        yc = cos(lat_c)*sin(lon_c)

        posVideocast = self.channelvideocastsdescription(channel)
        print posVideocast
        angleCV = []
        angleCV2 = []
        angle = []
        for i in range(0,len(posVideocast),1):
            beta = posVideocast[i]["gamma"]
            alpha = posVideocast[i]["alpha"]
            gamma = posVideocast[i]["gamma"]
            vx = -cos(alpha)*sin(gamma)-sin(alpha)*sin(beta)*cos(gamma)
            vy = -sin(alpha)*sin(gamma)+cos(alpha)*sin(beta)*cos(gamma)
            lat_v = posVideocast[i]["latitude"]
            latv = radians(lat_v)
            lon_v = posVideocast[i]["longitude"]
            lonv = radians(lon_v)
            angleV = atan(vx/vy)


            #cartesian coordinate point of videocast
            xv = cos(lat_v)*cos(lon_v)
            yv = cos(lat_v)*sin(lon_v)
            print xv,yv

            #videocast - target vector
            vecteurCVx = xc - xv
            vecteurCVy = yc - yv
            print 'les coordonnées x {0}, y {1}'.format(vecteurCVx,vecteurCVy)

            #Angle C-V beetween videocast target
            angleCV = atan(vecteurCVy/vecteurCVx) - atan(vy/vx)
            angle.append(angleCV)

            if angleCV <0:
               angleCV = angleCV +180

            angleCV2.append(angleCV)

        return angleCV2

    def videocastsReckon( self, target, channel):
        """calculate and select the main videocaster in channel
        @param target: target ID value
        @param channel: channel ID value
        @return: none
        """
        # variable globale
        addrdazzl = '52.48.244.76'
        logger = logging.getLogger('test')

        # Target position
        nayru = diane.SessionControl()
        targetdescribe = nayru.targetDescribe(target)
        targetposition = (targetdescribe["latitude"], targetdescribe["longitude"])
        lat_c = targetdescribe["latitude"]
        latc = radians(lat_c)
        lon_c = targetdescribe["longitude"]
        lonc = radians(lon_c)
        #cartesian coordinate point of target
        xc = cos(lat_c)*cos(lon_c)
        yc = cos(lat_c)*sin(lon_c)

        # join dazzel channel
        with dazzl.Session(addrdazzl,logger) as session:
            session.joinChannel(channel,'director')

            #get Videocaster IDs
            channelvideocasters = session.channelDescribe(channel)["videocasts"]
            videocasterids = []
            for videocast in channelvideocasters:
                videocasterids.append(videocast["id"])

            #get id videocaster closer
            videocastercloser = {"id": 000000, "distance": 99999}
            angleCV = []
            angleCV2 = []
            theta = 20
            for id in videocasterids:
                description = session.videocastDescribe(channel, id)
                #videocaster closer by distance
                videocasterposition = (description["latitude"], description["longitude"])
                distance = vincenty(targetposition, videocasterposition).km
                print "[{0}] {1} km".format(id,distance)

                #videocaster closer by angle
                alpha = (description["alpha"])
                beta = (description["beta"])
                gamma =(description["gamma"])
                vx = -cos(alpha)*sin(gamma)-sin(alpha)*sin(beta)*cos(gamma)
                vy = -sin(alpha)*sin(gamma)+cos(alpha)*sin(beta)*cos(gamma)
                lat_v = description["latitude"]
                latv = radians(lat_v)
                lon_v = description["longitude"]
                lonv = radians(lon_v)

                #cartesian coordinate point of videocast
                xv = cos(lat_v)*cos(lon_v)
                yv = cos(lat_v)*sin(lon_v)

                #videocast - target vector
                vecteurCVx = xc - xv
                vecteurCVy = yc - yv

                #Angle C-V beetween videocast target
                angleCV = atan(vecteurCVy/vecteurCVx) - atan(vy/vx)
                if angleCV <0:
                    angleCV = angleCV +180
                angleCV2.append(angleCV)
                print 'angle theta formé entre les 2 vecteurs CV et V est {0}'.format(angleCV)

                if videocastercloser["distance"] > distance and theta > angleCV:
                    videocastercloser = {"id": id, "distance": distance}

            # select id needed
            if session.channelDescribe(channel)["selected"]["id"] != videocastercloser["id"]:
                if videocastercloser["distance"] < 99999:

                    session.videocastSelect(channel, videocastercloser["id"])
                    print "videocaster [{0}] selected".format(videocastercloser["id"])
            else:
                print "closer videocaster [{0}] already selected".format(videocastercloser["id"])

            #Leave dazzl session
            session.leaveChannel()


    def targetdescription(self,targetId):
        """Describe a target

        Args:
            targetId: target ID

        Returns
            targetdescription: target description

        """
        sessiondiane = diane.SessionControl()
        targetdescription = sessiondiane.targetDescribe(targetId)
        return targetdescription


    def selectTargetView (self, targetId, channel):
        """select the videocast for the main view

        Args:
            targetId: target ID
            channel: channel ID
        """
        self.videocastsReckon(targetId,channel)







