"""
  Copyright (c) 2016 Sokhna Kandji and Kévin Felix
  Released under the MIT License (see license.txt)
"""
import argparse
import sys
import regina

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('command', choices=['selectview', 'list-videocasts', 'selectvideocast', 'describevideocasts','describetarget'],
                    help="Command to be executed on Regina")
parser.add_argument('channelId', type=int, nargs='?',
              help="Channel ID selects, describe commands")
parser.add_argument('targetId', type=int, nargs='?',
              help="Target ID for selects, describe commands")
parser.add_argument('videocastId', type=int, nargs='?',
              help="videocast ID for select commands")


def main(argv):

  # Parse the command-line flags.
  flags = parser.parse_args(argv[1:])

  # Check arguments consistency
  if flags.command in ['selectview'] and flags.channelId is None:
    parser.print_help()
    print 'Please provide a channel ID'
    return
  if flags.command in ['selectview'] and flags.targetId is None:
    parser.print_help()
    print 'Please provide a target ID'
    return
  if flags.command in ['describetarget'] and flags.targetId is None:
    parser.print_help()
    print 'Please provide a target ID'
    return
  if flags.command in ['describechannel'] and flags.channelId is None:
    parser.print_help()
    print 'Please provide a channel ID'
    return
  if flags.command in ['list-videocasts'] and flags.channelId is None:
    parser.print_help()
    print 'Please provide a channel ID'
    return


  # Create a Selecter
  selecter = regina.SessionRegina()
  
  
  # selectview
  if (flags.command == 'selectview'):
    create_args = {}
    if (flags.channelId is not None):
      create_args['channel'] = flags.channelId
    if (flags.channelId is not None):
      create_args['target'] = flags.targetId

    selecter.videocastsReckon(create_args['target'], create_args['channel'])
    return

  
  # list-videocasts
  if (flags.command == 'list-videocasts'):
    create_args = {}
    if (flags.channelId is not None):
      create_args['channel'] = flags.channelId
    print 'The videocasts ID are {0}'.format(selecter.channelVideocasterIds(create_args['channel']))
    return

  # describevideocasts
  if (flags.command == 'describevideocasts'):
    create_args = {}
    if (flags.channelId is not None):
      create_args['channel'] = flags.channelId

    print selecter.channelvideocastsdescription(create_args['channel'])
    return

  # describetarget
  if (flags.command == 'describetarget'):
    create_args = {}
    if (flags.targetId is not None):
      create_args['target'] = flags.targetId

    print 'Target {1} description :{0}'.format(selecter.targetdescription(create_args['target']),create_args['target'])
    return


if __name__ == '__main__':
  main(sys.argv)